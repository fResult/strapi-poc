'use strict';

/**
 * @param { import('webpack').Configuration } config - Webpack configuration object.
 * @param { import('webpack') } webpack - Webpack module.
 * @returns { import('webpack').Configuration } - Modified Webpack configuration object.
 */
module.exports = (config, webpack) => {
  // Note: we provide webpack above so you should not `require` it
  // Perform customizations to webpack config
  // Important: return the modified config
  return config;
};
